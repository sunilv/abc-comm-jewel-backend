from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url


urlpatterns = [
    path('admin/', admin.site.urls),
    # User routes
    path('api/jewels/', include('app.jewels.urls')),
    # User management url
    path('api/accounts/', include('app.accounts.urls')),

]