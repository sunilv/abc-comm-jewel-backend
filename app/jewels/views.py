from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework import generics
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework import status
from rest_framework.response import Response
from datetime import datetime, timedelta, date
from app.jewels.models import Product, Category, Wishlist
from app.jewels.serializers import ProductSerializer, CategorySerializer, WishlistSerializer
import math


@api_view(['GET','POST'])
@permission_classes((AllowAny,))
def products(request):
    if request.method == "GET":
        return get_products(request)
    
    if request.method == "POST":
        return create_or_products(request)

@api_view(['GET'])
@permission_classes((AllowAny,))
def categories(request):
    return get_categories(request)


@api_view(['GET'])
@permission_classes((AllowAny,))
def dashboard(request):
    return get_dashboard(request)

@api_view(['GET','POST','DELETE'])
@permission_classes((AllowAny,))
def wishlists(request):
    if request.method == "GET":
        return get_wishlists(request)
    if request.method == "POST":
        return create_wishlists(request)
    if request.method == "DELETE":
        return remove_wishlist(request)
    

def get_dashboard(request):
    '''
    Get Product and category count for dashboard purpose
    '''
    user = request.user
    product_count = Product.objects.all().count()
    category_count = Category.objects.all().count()
    return Response({
        'data': {
            'count': {
                'product' : product_count,
                'category' : category_count
            }
        },
        'code': 200,
      
    }, status=status.HTTP_200_OK)


def get_products(request):
    '''
    Get product list. 
    Param: Page, PerPage, filter by category
    '''
    user = request.user
    data = request.query_params
    filter_query = data.get('filter', None)
    page = int(data.get('page',1))
    per_page = int(data.get('per_page',10))
    old = (int(page) - 1) * per_page
    new = int(page) * per_page

    if filter_query is None:
        product_instances = Product.objects.all()
    else:
        category_list = filter_query.split(',')
        product_instances = Product.objects.filter(category__in=category_list)

    product_serializer = ProductSerializer(
        product_instances[old:new], many=True)
    max_pages = math.ceil(len(product_instances)/per_page)

    return Response({
        'data': product_serializer.data,
        'code': 200,
        "max_pages": max_pages
    }, status=status.HTTP_200_OK)


def get_categories(request):
    '''
    Get Category list. 
    '''
    user = request.user
   
    category_instances = Category.objects.all()

    category_serializer = CategorySerializer(
        category_instances, many=True)

    return Response({
        'data': category_serializer.data,
        'code': 200,
     
    }, status=status.HTTP_200_OK)


def get_wishlists(request):
    '''
    Get Wishlist list count and items
    '''
    user = request.user
    data = request.query_params
 
    wishlist_instances = Wishlist.objects.all()
    wishlist_serializer = WishlistSerializer(
        wishlist_instances, many=True)
    count = len(wishlist_instances)

    return Response({
        'code':200,
        'data': wishlist_serializer.data,
        'count': count,
    }, status=status.HTTP_200_OK)

def create_wishlists(request):
    '''
    Create new wishlist 
    Param: Page, PerPage, filter by category
    '''
    error =False
    error_message = None
    user = request.user
    data = request.data
    product = data.get('product',None)
    product_instance = Product.objects.get(id=product)
    new_wishlist = {
        "product": product_instance.id,
        "user": 1
    }
    wishlist_serializer = WishlistSerializer(data=new_wishlist)
    if wishlist_serializer.is_valid():
        wishlist_serializer.save()
    else: 
        error = True
        error_message = wishlist_serializer.errors

    if not error:
        return Response({
                'code': 200,
                'message': 'Added to your wishlist'
            }, status=status.HTTP_200_OK)
    else:
        return Response({
                'code': 400,
                'error': error_message,
                'message': "Error in adding wishlist",
            }, status=status.HTTP_400_BAD_REQUEST)


def remove_wishlist(request):
    error =False
    error_message = None
    user = request.user
    data = request.data

    wish_id  = data.get('wish',None)

    if wish_id:
        try:
            wishlist_instance = Wishlist.objects.get(id=wish_id)
            wishlist_instance.delete()
        except Wishlist.DoesNotExist:
            error = True
            error_message = "Wishlist not found"
    
    if not error:
        return Response({
                'code': 200,
                'message': 'Removed from your wishlist'
            }, status=status.HTTP_200_OK)
    else:
        return Response({
                'code': 400,
                'error': error_message,
                'message': "Error in removing wishlist",
            }, status=status.HTTP_400_BAD_REQUEST)


def create_or_products(request):
    '''
    Create New products
    '''
    error =False
    error_message = None
    user = request.user
    data = request.data

    product = data['products']

    new_products = format_product(product)
    product_serializer = ProductSerializer(data=new_products, many=True)
    if product_serializer.is_valid():
        product_serializer.save()
    else: 
        error = True
        error_message = product_serializer.errors

    if not error:
        return Response({
                'code': 200,
                'message': 'Product processed completed'
            }, status=status.HTTP_200_OK)
    else:
        return Response({
                'code': 400,
                'error': error_message,
                'message': "Error in adding products",
            }, status=status.HTTP_400_BAD_REQUEST)

def format_product(product_list):
    new_product_list = []
    for product in product_list:
        if product[0] == '_id':
            continue
        if product[7]:
            category = product[7]
        else:
            category = 'general'
        code = product[0]
        image = product[1]
        collection_name = product[8]
        description = product[14]
        availability = True if product[4] == "1" else False

        # Create Category
        category_instance = find_or_create_catgory(category) 

        new_product = {
        "code":code,
        "category": category_instance.id,
        "image" : image,
        "collection_name" : collection_name,
        "description" : description,
        "availability": availability,
        }
        new_product_list.append(new_product.copy())
    
    return new_product_list

def find_or_create_catgory(category):
    obj, created = Category.objects.get_or_create(name=category)
    return obj