from django.db import models
from django.contrib import admin
from app.accounts.models import User


class Category(models.Model):
    name = models.CharField(max_length=150, blank=True, null=False)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'category'


class Product(models.Model):
    code = models.CharField(max_length=100, blank=False, null=False, unique=True)
    category = models.ForeignKey(
        Category, on_delete=models.SET_NULL, null=True, blank=False)
    image = models.CharField(max_length=255, blank=True, null=True)
    collection_name = models.CharField(max_length=255, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    availability =models.BooleanField(default=False)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'product'

class Wishlist(models.Model):
    product = models.ForeignKey(
        Product, on_delete=models.SET_NULL, null=True, blank=False)
    user = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, blank=False)

    class Meta:
        db_table = 'wishlist'

admin.site.register(Product)
admin.site.register(Category)
admin.site.register(Wishlist)
