from django.conf.urls import url
from django.urls import path
from app.jewels import views as jewelView

urlpatterns = [
    # API Url's

    path('products', view=jewelView.products),
    path('categories', view=jewelView.categories),
    path('wishlists', view=jewelView.wishlists),
    path('dashboard', view=jewelView.dashboard),

]