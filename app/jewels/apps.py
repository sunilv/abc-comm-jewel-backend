from django.apps import AppConfig


class JewelsConfig(AppConfig):
    name = 'app.jewels'
