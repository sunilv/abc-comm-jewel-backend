from rest_framework import serializers
from django.utils import timezone

from app.jewels.models import Category, Product, Wishlist


class CategorySerializer(serializers.ModelSerializer):
    value = serializers.CharField(source='id')
    label = serializers.SerializerMethodField()

    def get_label(self,obj):
        return obj.name.replace('-',' ').title()

    class Meta:
        model = Category
        fields = ('id','label','value','name')


class ProductSerializer(serializers.ModelSerializer):
    category_name = serializers.SerializerMethodField()

    def get_category_name(self, obj):
        return obj.category.name.replace('-',' ').title()
    
    class Meta:
        model = Product
        fields = '__all__'


class WishlistSerializer(serializers.ModelSerializer):
    category_name = serializers.SerializerMethodField()
    product_description = serializers.SerializerMethodField()
    image = serializers.SerializerMethodField()
    
    
    def get_category_name(self, obj):
        return obj.product.category.name
    
    def get_product_description(self, obj):
        return obj.product.description

    def get_image(self, obj):
        return obj.product.image
  
    class Meta:
        model = Wishlist
        fields = '__all__'
        