from django.conf.urls import url
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from app.accounts import views as accountView
from rest_framework_jwt.views import verify_jwt_token


urlpatterns = [
    # API Url's

    path('auth/login', view=accountView.login),
    path('change-password', view=accountView.change_password),
    path('auth/otp-request', view=accountView.otp_request),
    path('auth/otp-verify', view=accountView.otp_verify),
    path('user-registration', view=accountView.registration),
    path('token-verify/', verify_jwt_token),
    path('me', view=accountView.me),
]
